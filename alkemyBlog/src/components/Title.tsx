import styled from "styled-components";

interface TitleProps {
  name: string;
  headerType: "h1" | "h2" | "h3" | "h4" | "h5" | "h6";
}
export const Title = ({ name, headerType }: TitleProps) => {
  const Wrapper = styled[headerType]`
    color: #17a2b8;
    font-weight: bold;
    text-shadow: 0 1px 1px #333;
    text-align: center;
    border-bottom: 1px solid #17a2b8;
    margin: 0;
    padding: 0;
  `;

  return (
    <a href="/home" className="plain">
      <Wrapper children={name} />
    </a>
  );
};
