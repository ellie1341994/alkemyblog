import * as React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import { Title } from "../Title";

export const Background = ({ children }: { children: any }) => {
  return (
    <Container fluid className="vh-100 bg-dark">
      <Row
        className="d-flex justify-content-center align-items-center w-100 h-25 p-0 m-0"
        children={<Title name="Alkemy Blog" headerType="h1" />}
      />
      <Row
        className="d-flex position-relative justify-content-center align-items-center w-100 h-75 p-0 m-0"
        children={children}
      />
    </Container>
  );
};
