import { IFormFields } from "../Form";
import axios, { AxiosResponse } from "axios";
export const submitForm = async (
  { email, password }: IFormFields,
  { setSubmitting, setFieldError }: any
) => {
  const URL: string = "http://challenge-react.alkemy.org";
  try {
    const request: AxiosResponse = await axios.post(URL, { email, password });
    if (request.status === 200) {
      localStorage.setItem("userToken", request.data.token);
      window.location.reload();
      setTimeout(() => {
        setSubmitting(false);
      }, 2000);
    }
  } catch (error) {
    const response = error.response;
    const errorMsg: string = error.response.data.error;
    console.log(response);
    error.response.status === 401 && setFieldError("generalErrors", errorMsg);
    console.log(error);
  }
};
