import { IFormFields } from "../Form";
export const checkFields = ({
  email,
  password,
  generalErrors,
}: IFormFields) => {
  const errors: any = {};
  // I wonder if the code below is bad practice but I wanted to try this instead of an if structure
  !email && (errors.email = "Required!");
  !password && (errors.password = "Required!");
  email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(email) &&
    (errors.email = "Invalid!");
  return errors;
};
