//import * as React from "react";
import { Formik } from "formik";
import { Background } from "../Background";
import ReactBootstrapForm from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { submitForm } from "./functionalities/submitForm";
import { checkFields } from "./functionalities/checkFields";
export interface IFormFields {
  email: string;
  password: string;
  generalErrors: string;
}
export const Form = () => (
  <Background>
    <Formik
      initialValues={{ email: "", password: "", generalErrors: "" }}
      validate={checkFields}
      onSubmit={submitForm}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => {
        return isSubmitting ? (
          <h2 className="text-white text-center">Logging In please wait ...</h2>
        ) : (
          <ReactBootstrapForm
            className="border border-info text-white w-50 m-5 p-5 rounded"
            style={{ boxSizing: "content-box" }}
            onSubmit={handleSubmit}
          >
            <ReactBootstrapForm.Group
              className="mb-2"
              controlId="formBasicEmail"
            >
              <ReactBootstrapForm.Label children={"Email"} />
              <span className="text-danger m-0 p-0">
                {errors.email && touched.email && " " + errors.email}
              </span>
              <ReactBootstrapForm.Control
                type="email"
                name="email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />
              <ReactBootstrapForm.Text className="text-muted">
                We'll never share your email with anyone else.
              </ReactBootstrapForm.Text>
            </ReactBootstrapForm.Group>
            <ReactBootstrapForm.Group
              className="mb-2"
              controlId="formBasicPassword"
            >
              <ReactBootstrapForm.Label children={"Password"} />
              <span className="text-danger m-0 p-0">
                {errors.password && touched.password && " " + errors.password}
              </span>
              <ReactBootstrapForm.Control
                type="password"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
            </ReactBootstrapForm.Group>
            <span className="text-danger m-0 p-0">
              {errors.generalErrors && " " + errors.generalErrors}
            </span>
            <Button
              type="submit"
              variant="info"
              className="font-weight-bold w-100"
              disabled={isSubmitting}
            >
              Submit
            </Button>
          </ReactBootstrapForm>
        );
      }}
    </Formik>
  </Background>
);
