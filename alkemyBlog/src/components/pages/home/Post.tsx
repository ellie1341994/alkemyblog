import * as React from "react";
// import ? as DeletePostButton from "./?"
// import ? as EditPostButton from "./?"
import { PostButton as DetailsButton } from "./PostButton";
export interface IPostData<T = string> {
  userId?: T;
  id?: T;
  title: string;
  body: string;
}
interface IPost extends IPostData {
  children: any;
}
export const Post = ({ userId, id, title, body, children }: IPost) => {
  const [showDetails, setShowDetails] = React.useState(false);
  const [showEditForm, setShowEditForm] = React.useState(false);
  return (
    <div className="bg-white shadow rounded mr-5 ml-5 mt-2 mb-2 p-1">
      <div className="w-100 d-flex justify-content-between align-items-center">
        <h6 className="hide-scrollbar text-nowrap text-nowrap d-inline p-0 m-0">
          {title}
        </h6>
        <div id="post-functions-wrapper" className="d-flex">
          <DetailsButton
            type="details"
            callback={() => !showEditForm && setShowDetails(!showDetails)}
          />
          {children}
        </div>
      </div>
      <div
        className={
          showDetails ? "mt-1 text-black border-top border-primary" : "d-none"
        }
      >
        {body}
      </div>
    </div>
  );
};
