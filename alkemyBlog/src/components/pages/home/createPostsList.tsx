import * as React from "react";
import { IPostData } from "./Post";
import { Post } from "./Post";
import { PostButton } from "./PostButton";
export function createPostsList(
  postsData: IPostData[],
  showPostEditModal: Function,
  deletePostInState: Function
) {
  const componentList: JSX.Element[] | any[] = [];
  let PostComponent: JSX.Element | undefined = undefined;
  postsData.forEach((postData: IPostData) => {
    if (postData) {
      PostComponent = (
        <Post
          key={"post-" + postData.id}
          body={postData.body}
          title={postData.title}
        >
          <PostButton
            postId={postData.id}
            type="delete"
            callback={deletePostInState}
          />
          <PostButton
            postId={postData.id}
            type="edit"
            callback={showPostEditModal}
          />
        </Post>
      );
      componentList.push(PostComponent);
    }
  });
  return componentList;
}
