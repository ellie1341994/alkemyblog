import * as React from "react";
import { Background } from "../Background";
import { requestPosts } from "./requestPosts";
import { createPostsList } from "./createPostsList";
import { BasePostForm } from "./BasePostForm";
import { NavBar } from "./NavBar";
/**
 * Handles blog state and general layout
 */
interface IModal {
  postIdToEdit: string | undefined;
  createPost: boolean;
}
export const Blog = () => {
  /* the following way that the state is being manage assumes that:
     entry ids on a database's table for posts always increment by one continously, do not reuse deleted ids
     or updates them after each post deletion 
  */
  const localPosts = JSON.parse(localStorage.getItem("localPosts") ?? "[null]");
  const [posts, setPosts] = React.useState(localPosts);
  const [showModal, setShowModal] = React.useState<IModal>({
    postIdToEdit: undefined,
    createPost: false,
  });
  /* note: an empty null data is given as a default state value so
     array indexes matches post ids 
  */
  const initiateState = () => {
    const getStateData = async () => {
      if (localPosts.length <= 1) {
        const data = await requestPosts();
        setPosts(posts.concat(data));
      } else {
        setPosts(posts);
      }
    };
    getStateData();
  };
  React.useEffect(initiateState, []);
  function createPostInState(postData: any) {
    if (postData) {
      setPosts((currentPosts: any[]) => {
        const newState = [...currentPosts];
        newState.push(postData);
        console.log(newState);
        return newState;
      });
      setShowModal({
        postIdToEdit: undefined,
        createPost: false,
      });
    }
  }
  function deletePostInStateHandler(postId: string) {
    const validPostId: boolean = /^\d+$/.test(postId);
    if (validPostId) {
      setPosts((currentPosts: any[]) => {
        const index: number = parseInt(postId);
        const newState = [...currentPosts];
        newState[index] = null;
        return newState;
      });
    }
    return validPostId ? "Deleting" : "Couldn't delete. Try later";
  }
  function editPostInState(postId: string, postData: any) {
    const validPostId: boolean = /^\d+$/.test(postId);
    if (validPostId) {
      setPosts((currentPosts: any[]) => {
        const index: number = parseInt(postId);
        const newState = [...currentPosts];
        newState[index].title = postData.title;
        newState[index].body = postData.body;
        return newState;
      });
    }
    setShowModal({ postIdToEdit: undefined, createPost: false });
  }
  function setLocalPosts() {
    localStorage.setItem("localPosts", JSON.stringify(posts));
  }
  React.useEffect(() => {
    setLocalPosts();
  }, [posts]);
  // PostLists recreates the list on every render which is not optimal
  // but i don't have enough time to change it
  const reversedPosts = [...posts].reverse();
  let PostsList = () =>
    createPostsList(
      reversedPosts,
      (postId: string) =>
        setShowModal({ postIdToEdit: postId, createPost: false }),
      deletePostInStateHandler
    );
  const formInitialValues = {
    title: "",
    body: "",
    userId: posts[1]?.userId,
    id: posts.length,
    generalErrors: "",
  };
  if (showModal.postIdToEdit) {
    formInitialValues.title = posts[showModal.postIdToEdit!].title;
    formInitialValues.body = posts[showModal.postIdToEdit!].body;
    formInitialValues.userId = undefined;
  }
  return (
    <Background>
      {showModal.postIdToEdit !== undefined && (
        <BasePostForm
          type="edit"
          initialValues={formInitialValues}
          callback={editPostInState}
          postId={showModal.postIdToEdit}
        />
      )}
      {showModal.createPost && (
        <BasePostForm
          type="create"
          initialValues={formInitialValues}
          callback={createPostInState}
        />
      )}
      <div id="blog-wrapper" className="d-flex  flex-column w-100 h-100 mb-1">
        <NavBar callback={setShowModal} />
        <div id="posts-wrapper" className="hide-scrollbar w-100 h-100 mb-1">
          {PostsList()}
        </div>
      </div>
    </Background>
  );
};
