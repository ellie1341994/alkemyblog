import * as React from "react";
export const NavBar = ({ callback }: { callback: Function }) => {
  return (
    <div className="d-flex justify-content-evenly">
      <a
        href="#"
        className="text-info plain font-weight-bold fs-4"
        onClick={(event) => {
          event.preventDefault();
          callback({ postIdToEdit: undefined, createPost: true });
        }}
      >
        Publish
      </a>
      <a
        className="text-info plain font-weight-bold fs-4"
        onClick={(event: any) => {
          localStorage.removeItem("userToken");
        }}
        href="/"
      >
        Logout
      </a>
    </div>
  );
};
