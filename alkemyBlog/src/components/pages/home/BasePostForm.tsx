//import * as React from "react";
import { Formik } from "formik";
import ReactBootstrapForm from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
//import { submitForm } from "./function/submitForm";
import { checkFields } from "./functions/checkFields";
import axios, { AxiosResponse } from "axios";
export interface IFormInitialValues {
  title: string;
  body: string;
  userId?: string | undefined;
  id?: string | undefined;
  generalErrors: string;
}
export interface IBasePostForm {
  type: "edit" | "create";
  initialValues: IFormInitialValues;
  callback: Function;
  postId?: string;
}
export const BasePostForm = ({
  type,
  initialValues,
  callback,
  postId,
}: IBasePostForm) => (
  <Formik
    initialValues={initialValues}
    validate={checkFields}
    onSubmit={async (
      { title, body, id, userId }: IFormInitialValues,
      { setSubmitting, setFieldError }: any
    ) => {
      const URL: string = "https://jsonplaceholder.typicode.com/posts";
      try {
        if (type === "edit") {
          const request: AxiosResponse = await axios.put(URL + `/${postId}`, {
            title,
            body,
          });
          request.status === 200 && callback(postId, { title, body });
        } else if (type === "create") {
          const request: AxiosResponse = await axios.post(URL, {
            title,
            body,
            userId,
            id,
          });
          console.log(request);
          request.status === 201 && callback({ title, body, userId, id });
        }
      } catch (error) {
        const response = error.response;
        console.log(response);
        error.response.status === 401 &&
          setFieldError("generalErrors", "Unknown error");
        console.log(error);
      }
    }}
  >
    {({
      values,
      errors,
      touched,
      handleChange,
      handleBlur,
      handleSubmit,
      isSubmitting,
    }) => {
      return (
        <div className="d-flex justify-content-center align-items-center opaque-background position-absolute top-0 bottom-0">
          {isSubmitting ? (
            <h2 className="fs-2 text-warning text-center">Please wait ...</h2>
          ) : (
            <ReactBootstrapForm
              className="border border-info text-white w-sm-50 w-md-25 m-5 p-5 rounded bg-dark"
              style={{ boxSizing: "content-box" }}
              onSubmit={handleSubmit}
            >
              <ReactBootstrapForm.Group
                className="mb-2"
                controlId="formBasicEmail"
              >
                <ReactBootstrapForm.Label children={"Title"} />
                <span className="text-danger m-0 p-0">
                  {errors.title && touched.title && " " + errors.title}
                </span>
                <ReactBootstrapForm.Control
                  type="text"
                  name="title"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.title}
                />
              </ReactBootstrapForm.Group>
              <ReactBootstrapForm.Group
                className="mb-2"
                controlId="formBasicPassword"
              >
                <ReactBootstrapForm.Label children={"Text"} />
                <span className="text-danger m-0 p-0">
                  {errors.body && touched.body && " " + errors.body}
                </span>
                <ReactBootstrapForm.Control
                  as="textarea"
                  name="body"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.body}
                />
              </ReactBootstrapForm.Group>
              <span className="text-danger m-0 p-0">
                {errors.generalErrors && " " + errors.generalErrors}
              </span>
              <Button
                type="submit"
                variant="info"
                className="font-weight-bold w-100 text-capitalize"
                disabled={isSubmitting}
              >
                {type}
              </Button>
            </ReactBootstrapForm>
          )}
        </div>
      );
    }}
  </Formik>
);
