import { IFormInitialValues } from "../BasePostForm";
export const checkFields = ({
  title,
  body,
  generalErrors,
}: IFormInitialValues) => {
  const errors: any = {};
  // I wonder if the code below is bad practice but I wanted to try this instead of an if structure
  !title && (errors.title = "Required!");
  !body && (errors.body = "Required!");
  console.log(errors);
  return errors;
};
