import * as React from "react";
import Button from "react-bootstrap/Button";
import axios from "axios";
import { ImCross } from "react-icons/im";
import { RiPencilFill } from "react-icons/ri";
import { FiBookOpen } from "react-icons/fi";

/**
 * @param type determines functionality, styles and button name as fallback
 * @param endpoint is the url that the button will execute the request
 * @param callback function related to the button type for further processing besides the button own functionality
 */
interface IPostButton<T = string> {
  type?: "delete" | "edit" | "details";
  callback?: Function;
  postId?: T;
}
export const PostButton = ({
  type,
  callback = () => {},
  postId,
}: IPostButton) => {
  function deletePost(event: React.MouseEvent<HTMLElement>) {
    event.preventDefault();
    axios["delete"]("https://jsonplaceholder.typicode.com/posts/" + postId)
      .then((response) => {
        if (response.status === 200) {
          callback(postId);
        }
      })
      .catch((error) => console.log(error));
  }
  // defaults to type details configuration
  let ButtonIcon = FiBookOpen;
  if (type === "delete") {
    ButtonIcon = ImCross;
  } else if (type === "edit") {
    ButtonIcon = RiPencilFill;
  }
  return (
    <Button
      onClick={type === "delete" ? deletePost : () => callback(postId)}
      className="p-1 m-1"
      variant={"outline-light"}
      children={<ButtonIcon color="black" size={12} /> ?? type}
    />
  );
};
