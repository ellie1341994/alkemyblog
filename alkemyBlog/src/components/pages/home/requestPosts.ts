import axios from "axios";
export async function requestPosts() {
  try {
    const request = await axios["get"](
      "https://jsonplaceholder.typicode.com/posts/"
    );
    if (request.status === 200) {
      console.log("executes");
      return request.data;
    }
  } catch (error) {
    console.log(error);
  }
}
