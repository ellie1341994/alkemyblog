import { Helmet, HelmetProvider } from "react-helmet-async";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import "./App.css";
import { Blog as HomePage } from "./components/pages/home/Blog";
import { Form as LoginPage } from "./components/pages/login/Form";

const App = () => {
  const userToken = localStorage.getItem("userToken");
  return (
    <HelmetProvider>
      <Helmet title="AlkemyBlog" />
      <Router>
        <Route
          path="/"
          render={({ location }) => {
            const userAuthenticated: boolean = Boolean(userToken);
            const isUserLoggingIn: boolean =
              !userAuthenticated && /^\/login/.test(location.pathname);
            const isUserBlogging: boolean =
              userAuthenticated && /^\/home/.test(location.pathname);
            const redirectUser: boolean = !isUserLoggingIn && !isUserBlogging;
            if (redirectUser) {
              return <Redirect to={userAuthenticated ? "/home" : "/login"} />;
            }
            return (
              <Route
                exact
                path={location.pathname}
                render={() =>
                  userAuthenticated ? <HomePage /> : <LoginPage />
                }
              />
            );
          }}
        />
      </Router>
    </HelmetProvider>
  );
};

export default App;
